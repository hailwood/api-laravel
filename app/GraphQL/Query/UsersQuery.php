<?php

namespace App\GraphQL\Query;

use App\GraphQL\Type\UserNode;
use App\GraphQL\Type\UsersConnection;
use App\User;
use Folklore\GraphQL\Support\Query as BaseQuery;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;

class UsersQuery extends BaseQuery
{
    protected $attributes = [
        'name' => 'UsersQuery',
        'description' => 'A query'
    ];

    protected function type()
    {
        return Type::listOf(GraphQL::type('UsersConnection'));
    }

    protected function args()
    {
        return [
            
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        die(get_class($root));
        return User::get();
    }
}
