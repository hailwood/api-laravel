<?php

namespace App\GraphQL\Query;

use App\User;
use Folklore\GraphQL\Support\Query as BaseQuery;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;

class UserQuery extends BaseQuery
{
    protected $attributes = [
        'name' => 'UsersQuery',
        'description' => 'A query'
    ];

    protected function type()
    {
        return GraphQL::type('UserNode');
    }

    protected function args()
    {
        return [
            
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        return User::first();
    }
}
