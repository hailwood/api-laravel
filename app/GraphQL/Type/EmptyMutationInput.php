<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Relay\Support\InputType as BaseInputType;
use GraphQL;

class EmptyMutationInput extends BaseInputType
{
    protected $attributes = [
        'name' => 'EmptyMutationInput',
        'description' => 'A relay mutation input type'
    ];

    protected function fields()
    {
        return [
        
        ];
    }
}
