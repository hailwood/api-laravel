<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Relay\Support\PayloadType as BasePayloadType;
use GraphQL;

class EmptyMutationPayload extends BasePayloadType
{
    protected $attributes = [
        'name' => 'EmptyMutationPayload',
        'description' => 'A relay mutation payload type'
    ];

    protected function fields()
    {
        return [
        
        ];
    }
}
