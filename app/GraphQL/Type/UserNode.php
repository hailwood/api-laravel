<?php

namespace App\GraphQL\Type;

use App\User;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Relay\Support\NodeType as BaseNodeType;
use GraphQL;

class UserNode extends BaseNodeType
{
    protected $attributes = [
        'name'        => 'UserNode',
        'description' => 'A relay node type',
    ];

    protected function fields()
    {
        return [
            'id'    => [
                'type' => Type::nonNull(Type::id()),
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'name'  => [
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    public function resolveById($id)
    {
        return User::find($id);
    }
}
