<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Relay\Support\ConnectionType as BaseConnectionType;
use GraphQL;

class UsersConnection extends BaseConnectionType
{
    protected $attributes = [
        'name' => 'UsersConnection',
        'description' => 'A connection to all users'
    ];

    protected function edgeType()
    {
        return GraphQL::type('UserNode');
    }
}
