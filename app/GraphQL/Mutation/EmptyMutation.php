<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Relay\Support\Mutation as BaseMutation;
use GraphQL;

class EmptyMutation extends BaseMutation
{
    protected $attributes = [
        'name' => 'EmptyMutation',
        'description' => 'An empty relay mutation'
    ];

    protected function inputType()
    {
        return GraphQL::type('EmptyMutationInput');
    }
    
    protected function type()
    {
        return GraphQL::type('EmptyMutationPayload');
    }
    
    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        
    }
}
